package com.ehy.archetype.application.dao.impl;

import com.ehy.archetype.application.dao.interfaces.SystemParameterRepository;
import com.ehy.archetype.application.entity.domain.SystemParameter;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class SystemParameterRepositoryImpl extends BaseRepositoryImpl<SystemParameter> implements SystemParameterRepository {

}

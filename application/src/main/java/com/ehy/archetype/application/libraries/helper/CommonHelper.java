package com.ehy.archetype.application.libraries.helper;

import com.ehy.archetype.application.entity.enums.Action;
import com.ehy.archetype.application.entity.domain.BaseMongo;
import com.ehy.common.model.MandatoryRequest;
import java.time.Instant;

public class CommonHelper {

  private CommonHelper() {
  }

  public static <T extends BaseMongo> T setBaseMongoFields(MandatoryRequest mandatoryRequest,
      T object, Action action) {
    Instant now = Instant.now();
    switch (action) {
      case CREATE:
        object.setCreatedBy(mandatoryRequest.getUsername());
        object.setUpdatedBy(mandatoryRequest.getUsername());
        object.setCreatedAt(now);
        object.setUpdatedAt(now);
        object.setDeleted(false);
        break;
      case UPDATE:
        object.setUpdatedBy(mandatoryRequest.getUsername());
        object.setUpdatedAt(now);
        object.setDeleted(false);
        break;
      case DELETE:
        object.setUpdatedBy(mandatoryRequest.getUsername());
        object.setUpdatedAt(now);
        object.setDeleted(true);
        break;
    }

    return object;
  }
}

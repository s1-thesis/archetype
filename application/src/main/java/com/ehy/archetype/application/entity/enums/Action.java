package com.ehy.archetype.application.entity.enums;

public enum Action {
  CREATE, UPDATE, DELETE
}

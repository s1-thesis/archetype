package com.ehy.archetype.application.service.interfaces;

import com.ehy.archetype.application.entity.domain.BaseMongo;
import com.ehy.common.model.MandatoryRequest;
import com.ehy.common.model.PageWrapper;
import io.smallrye.mutiny.Uni;

public interface BaseResourceService<T extends BaseMongo> {

  Uni<PageWrapper<T>> findAll(Integer page, Integer size);

  Uni<T> findById(String id);

  Uni<Boolean> create(MandatoryRequest mandatoryRequest, T systemParameter);

  Uni<Boolean> update(MandatoryRequest mandatoryRequest, T systemParameter);

  Uni<Boolean> delete(MandatoryRequest mandatoryRequest, String id);
}

package com.ehy.archetype.application.service.interfaces;

import com.ehy.archetype.application.entity.domain.SystemParameter;

public interface SystemParameterService extends BaseResourceService<SystemParameter> {

}

package com.ehy.archetype.application.dao.interfaces;

import com.ehy.archetype.application.entity.domain.SystemParameter;

public interface SystemParameterRepository extends BaseRepository<SystemParameter> {

}
